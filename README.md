# bt

Wrap bluetoothctl to manage MAC addresses for you.

## Installation

```bash
git clone https://git.envs.net/lel/bt
cd bt
ln -s $(pwd)/bt ~/.local/bin
```

## Usage

### But first!

To start using `bt` you need to pair a device in bluetoothctl and get its MAC address. This is an occasionally fickle process that is outside the scope of `bt`, so it isn't included, but here's an example session of me connecting to my galaxy buds:

First, get your bluetooth device into its pairing mode (how this is done varies wildly), then something like this:

```
bluetoothctl
[bluetooth]# power on
Changing power on succeeded
[bluetooth]# agent on
Agent is already registered
[bluetooth]# scan on
Discovery started
[CHG] Controller 9C:B6:D0:93:E7:B8 Discovering: yes
[NEW] Device EC:AA:25:7A:13:66 Galaxy Buds (1366)
[bluetooth]# pair EC:AA:25:7A:13:66
Attempting to pair with EC:AA:25:7A:13:66
[garbage snipped...]
[CHG] Device EC:AA:25:7A:13:66 Paired: yes
Pairing successful
[Galaxy Buds (1366)]# quit
```

Copy that MAC address for use with bt in a second.

### Actual usage:

```bash
bt [options] device_name
```

where `device_name` is a name to represent the device.

To add a new device by name and MAC address:

```bash
bt -n EC:AA:25:7A:13:66 buds
```

To connect to a device you have added:

```bash
bt buds
```

Note once again that to connect like this you will need to be paired in bluetoothctl, which you'll only need to do once (see above).

-d disconnects from a device by name:

```bash
bt -d buds
```

-r removes a device from being tracked by bt, but *does not* unpair it in bluetoothctl

```bash
bt -r buds
```

To actually unpair it from bluetoothctl, do `bluetoothctl remove EC:AA:25:7A:13:66`. If you don't remember the MAC address, look around in `bluetoothctl devices`.

Finally, -h prints a help text that explains all of this.

## Misc. notes

This is really bad and I know that but I don't care. I figured maybe someone would find it useful so here it is. I haven't tested this outside my own setup at all and it's entirely possible that there will be little problems that will require you to edit it slightly. idk.
